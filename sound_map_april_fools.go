package main

import (
	"math/rand"
	"reflect"
	"time"
)

// GetRandomSound returns a random sound
func (s *SoundMap) GetRandomSound() ([][]byte, time.Time, error) {
	keys := reflect.ValueOf(s.entries).MapKeys()
	e := s.entries[keys[rand.Intn(len(keys))].Interface().(string)]
	return get(e)
}
