package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
)

type Sound struct {
	UserID   string `json:"user_id"`
	Location string `json:"location"`
	Type     string `json:"type"`
}
type SoundConf struct {
	Sounds []Sound `json:"sounds"`
}

type SoundLoader struct {
	Logger logrus.FieldLogger
}

func NewSoundLoader(logger logrus.FieldLogger) *SoundLoader {
	return &SoundLoader{Logger: logger.WithField("comp", "sound_loader")}
}

func (loader *SoundLoader) LoadConfig(addr string) (SoundConf, error) {
	resp, err := http.Get(addr)
	if err != nil {
		return SoundConf{}, errors.Wrapf(err, "could not get sound conf from %s", addr)
	}
	var soundConf *SoundConf
	if resp.StatusCode != 200 {
		return SoundConf{}, errors.New("Response Status not 200")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &soundConf)
	if err != nil {
		return SoundConf{}, errors.Wrap(err, "invalid sound conf format")
	}
	return *soundConf, nil
}

//LoadConfigSounds loads all Sounds from specified sources
func (loader *SoundLoader) LoadConfigSounds(conf SoundConf, soundMap *SoundMap) error {
	var err error
	for _, s := range conf.Sounds {
		var sound [][]byte
		switch s.Type {
		case "file":
			sound, err = loadSoundFromFile(s.Location)
			if err != nil {
				return errors.Wrap(err, fmt.Sprintf("could not load sound from %s", s.Location))
			}
			loader.Logger.WithField("location", s.Location).Debug("Loaded Sound")
			break
		case "url":
			sound, err = loadSoundFromURL(s.Location)
			if err != nil {
				return errors.Wrap(err, fmt.Sprintf("could not load sound from %s", s.Location))
			}
			loader.Logger.WithField("location", s.Location).Debug("Loaded Sound")
			break
		default:
			panic("Unknown Sound Data Type " + s.Type)
		}
		soundMap.Set(s.UserID, sound)
	}
	return nil
}
