package main

import (
	"regexp"
	"strconv"
	"testing"
)

// TestRollUserMsg tests the rollUserMsg function
// It tests whether we get exactly three distinct possible values
func TestUserMsg(t *testing.T) {
	var foundValues = make(map[int]struct{})
	var re = regexp.MustCompile(`rolled (\d+) \(0-100\)`)
	for i := 0; i < 1000; i++ {
		testStr := rollMean("abc")
		matches := re.FindStringSubmatch(testStr)
		rolledValue, err := strconv.Atoi(matches[1])
		if err != nil {
			t.Errorf("Failed to parse rolled value: %s", err)
		}
		foundValues[rolledValue] = struct{}{}
	}
	if len(foundValues) != 3 {
		t.Errorf("Expected 3 unique values, got %d", len(foundValues))
	}
}
