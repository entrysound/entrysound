package main

import (
	"fmt"
	mathrand "math/rand"
	"os"
	"strings"
	"sync"

	"github.com/bwmarrin/discordgo"
)

var (
	MeanRollingEnabled = true
)

var (
	allowedResults     map[string][]uint = make(map[string][]uint)
	allowedResultsLock sync.Mutex
)

func init() {
	meanRolling := os.Getenv("MEAN_ROLLING_ENABLED")
	if meanRolling != "" {
		MeanRollingEnabled = true
	} else {
		MeanRollingEnabled = false
		fmt.Println("Mean rolling has been disabled")
	}
}

func text(discord *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == discord.State.User.ID {
		return
	}
	if m.Content == "!roll" {
		var rollText string
		if MeanRollingEnabled {
			rollText = rollMean(m.Author.ID)
		} else {
			rollText = rollClassic(m.Author.ID)
		}
		discord.ChannelMessageSend(m.ChannelID, rollText)
	}
	if m.Content == "!rollall" {
		rollall(discord, m.ChannelID)
	}
}

func rollall(discord *discordgo.Session, channel string) {
	c, _ := discord.Channel(channel)
	rollMsgs := make([]string, 0)
	for _, user := range c.Recipients {
		rollMsgs = append(rollMsgs, rollClassic(user.ID))
	}
	sendMsg := strings.Join(rollMsgs, "\n")
	logger := ProvideLogger()
	logger.Printf("Sending MSG: \n" + sendMsg + "\n")
	discord.ChannelMessageSend(channel, strings.Join(rollMsgs, "\n"))
}

func rollClassic(userId string) string {
	return fmt.Sprintf("<@%s> rolled %d (0-100)", userId, mathrand.Intn(101))
}

func rollMean(userId string) string {
	allowedResultsLock.Lock()
	defer allowedResultsLock.Unlock()
	allowedResultsForPlayer, ok := allowedResults[userId]
	if !ok {
		allowedResults[userId] = make([]uint, 0)
		allowedResultsForPlayer = allowedResults[userId]
	}
	var randomIndex int
	if len(allowedResultsForPlayer) == 3 {
		randomIndex = mathrand.Intn(len(allowedResultsForPlayer))
	} else {
		allowedResultsForPlayer = append(allowedResultsForPlayer, uint(mathrand.Int31n(101)))
		randomIndex = mathrand.Intn(len(allowedResultsForPlayer))
		allowedResults[userId] = allowedResultsForPlayer
	}
	randomValue := allowedResultsForPlayer[randomIndex]

	return fmt.Sprintf("<@%s> rolled %d (0-100)", userId, randomValue)
}
