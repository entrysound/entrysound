FROM golang:1.18
WORKDIR /entrysound
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build
CMD [ "./entrysound" ]
