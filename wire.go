//+build wireinject

package main

import (
	"os"
	"time"

	"github.com/go-redsync/redsync"
	"github.com/gomodule/redigo/redis"
	"github.com/google/wire"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var mainSet = wire.NewSet(NewSoundMap, NewSoundLoader, ProvideLogger)
var redisSet = wire.NewSet(ProvideRedisURLGetter, NewRedis, NewRedsync, ProvideLogger)

func ProvideSoundLoader() *SoundLoader {
	wire.Build(mainSet)
	return &SoundLoader{}
}

func ProvideLogger() logrus.FieldLogger {
	logger := logrus.New()
	logger.SetOutput(os.Stdout)
	logger.SetLevel(logrus.DebugLevel)
	logger.SetFormatter(&logrus.JSONFormatter{})
	return logger.WithField("app", "entrysound")
}

type redisURLGetter func() string

func ProvideRedisURLGetter() redisURLGetter {
	return redisURLGetter(func() string {
		return *redisURL
	})
}
func NewRedis(redisURL redisURLGetter, logger logrus.FieldLogger) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", redisURL())
			if err != nil {
				logger.WithField("comp", "redis").WithError(err).Error("Could not connect to redis")
				return nil, errors.Wrapf(err, "could not dial %s", redisURL())
			}
			return c, nil
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func NewRedsync(pool *redis.Pool) *redsync.Redsync {
	return redsync.New([]redsync.Pool{pool})
}

func ProvideRedsync() *redsync.Redsync {
	wire.Build(redisSet)
	return nil
}

func ProvideRedis() *redis.Pool {
	wire.Build(redisSet)
	return nil
}
